package net.iescierva.dam17_01.a06comunicacionactividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {


    Button aceptar, rechazar;
    TextView saludo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        aceptar =(Button)findViewById(R.id.AceptarBtn);
        rechazar =(Button)findViewById(R.id.RechazarBtn);
        saludo =(TextView)findViewById(R.id.SaludoTv);

        Intent intentRecibido = getIntent();
        String nombre = "";
        if(intentRecibido != null){
            nombre = intentRecibido.getStringExtra("nombre");
        }

        saludo.setText("Hola "+ nombre + ", ¿Aceptas las condiciones?");

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent();
                data.putExtra("respuesta", "Aceptado");
                setResult(RESULT_OK, data);
                finish();
            }
        });

        rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent();
                data.putExtra("respuesta", "Rechazado");
                setResult(RESULT_OK, data);
                finish();
            }
        });
    }
}