package net.iescierva.dam17_01.a06comunicacionactividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button siguiente;
    EditText nombre;
    TextView decision;

    int ARRANCAR = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        siguiente =(Button)findViewById(R.id.VerificarBtn);
        nombre =(EditText)findViewById(R.id.NombreEt);
        decision =(TextView) findViewById(R.id.ResultadoTv);


        siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent siguiente = new Intent(MainActivity.this, Main2Activity.class);
                siguiente.putExtra("nombre",nombre.getText().toString());
                startActivityForResult(siguiente, ARRANCAR);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ARRANCAR && resultCode == RESULT_OK) {

                String respuesta = data.getStringExtra("respuesta");

                MainActivity.this.decision.setText("Resultado: " +respuesta);

        }
    }
}